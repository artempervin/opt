#include <list>
#include <algorithm>
#include <iostream>

template <typename T>
std::list<T> seq_quick_sort(std::list<T> input)
{
   if (input.empty())
      return input;

   std::list<T> result;
   result.splice(result.begin(), input, input.begin());

   T const& pivot = *result.begin();

   auto divide_point = std::partition(input.begin(), input.end(),
                                      [&](T const& t){ return t < pivot; });

   std::list<T> lower_part;
   lower_part.splice(lower_part.end(), input, input.begin(), divide_point);

   auto new_lower = seq_quick_sort(std::move(lower_part));
   auto new_higher = seq_quick_sort(std::move(input));

   result.splice(result.end(), new_higher);
   result.splice(result.begin(), new_lower);

   return result;
}

int main()
{
   std::list<int> numbers = {3, 1, 5, 0, 9, 1, 7, 2};
   auto sorted = seq_quick_sort(numbers);
   for (auto& i : sorted)
      std::cout << i << " ";
   std::cout << std::endl;
}
