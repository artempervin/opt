#include <cstdlib>
#include <iostream>

int myrand(int low, int high)
{
    int range = high - low;
    return range > 0 ? rand() % range + low : low;
}

void do_k(int& k, int low, int high)
{
    if (!k)
        return;

    --k;

    if (low  == high)
        std::cerr << low << " ";
    else
    {
        int rand = myrand(low, high);
        std::cerr << rand << " ";

        if (rand == low)
            do_k(k, rand+1, high);
        else
        {
            do_k(k, low, rand-1);
            do_k(k, rand+1, high);
        }
    }
}

void k_unique(int k, int n)
{
    do_k(k, 0, n);
    std::cerr << std::endl;
}

int main()
{
    srand(time(0));

    k_unique(10, 10);
    k_unique(3, 10);
    k_unique(1, 1);
}
