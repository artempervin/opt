#include <cassert>

template <template <typename> class Functor, typename T>
void apply(T& v)
{
    Functor<T> f;
    f(v);
}

template <template <typename> class Functor, typename T, typename... Args>
void apply(T& first, Args&... args)
{
    Functor<T> f;
    f(first);
    apply<Functor>(args...);
}

struct Base
{
    Base() : i(0) {}
    int i;
};

struct A: Base
{
    void f()
    {
        ++i;
    }
};

struct B: Base
{
    void f()
    {
        i = 100;
    }
};

struct C: Base
{
    void f()
    {
        --i;
    }
};


template <typename T>
struct F
{
    void operator()(T& v)
    {
        v.f();
    }
};

int main()
{
    A a;
    B b;
    C c;

    apply<F>(a, b, c);
    
    assert(a.i == 1);
    assert(b.i == 100);
    assert(c.i == -1);
}
