// compile with: clang++ --std=c++1y apply_print.cpp.cpp

#include <iostream>

template <typename F>
void apply(F func)
{
}

template <typename F, typename T, typename... Ts>
void apply(F func, T arg, Ts... args)
{
    func(arg);
    apply(func, args...);
}

int main()
{
    auto print = [](auto v){std::cout << v << " ";};

    apply(print, "lala", "blabla"); std::cout << std::endl;
    apply(print, 1); std::cout << std::endl;
    apply(print, "Hello, World"); std::cout << std::endl;
}
