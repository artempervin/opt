; make it easy on eyes first ...
(set-foreground-color "gray")
(set-background-color "black")

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(setq font-lock-maximum-size 262144)

;; none of these please
(scroll-bar-mode nil)
(tool-bar-mode nil)
(menu-bar-mode nil)

;; ruby-specific modes
(setq auto-mode-alist
      (append '(("\\.rb$" . ruby-mode)) auto-mode-alist))
(setq auto-mode-alist
      (append '(("\\.rjs$" . ruby-mode)) auto-mode-alist))
(setq auto-mode-alist
      (append '(("\\.rake$" . ruby-mode)) auto-mode-alist))
(setq auto-mode-alist
      (append '(("Rakefile$" . ruby-mode)) auto-mode-alist))

;; c/c++ specific modes
;; set stroustrup style
(defun my-linux-c-mode ()
 (c-set-style "stroustrup")
 (setq-default indent-tabs-mode nil) ; disable tabs
)

(setq auto-mode-alist
      (append '(("\\.h$" . c++-mode)) auto-mode-alist))
(add-hook 'c++-mode-hook 'my-linux-c-mode)
(add-hook 'c-mode-hook 'my-linux-c-mode)

;; add support for viewing chm files
;(require 'chm-view)

(setq browse-url-browser-function 'w3m-browse-url)

; nxml mode for xml files
;(load "/home/artemp/tmp/nxml-mode-20041004/rng-auto.el")

(setq auto-mode-alist '(
                        ("\\.\\(tk\\|tcl\\)\\'"                     . tcl-mode)
                        ("\\.pl\\'"                                 . perl-mode)
                        ("\\.emacs\\'"                              . lisp-mode)
                        ("\\.\\([Cc]\\|[hH]\\|cpp\\|cc\\|hpp\\)\\'" . c++-mode)
                        ("\\.\\(sh\\|bash\\)"                       . sh-mode)
                        ("[Mm]akefile"                              . makefile-mode)
                        ("\\.\\(xml\\|xsl\\|rng\\|xhtml\\)\\'"      . nxml-mode)
                        ("\\.py\\'"                                 . python-mode)   
                       )
)

;; German's buffer title renamer
(setq frame-title-format '(buffer-file-name "%f" ("%b")))
(put 'upcase-region 'disabled nil)

;; Suppress welcome screen
(setq inhibit-startup-message t)

;; add line numbers on the left
;(require 'linum)
;(add-hook 'find-file-hook (lambda () (linum-mode 1)))
;(global-linum-mode 1)
;; enable line numbers
(global-linum-mode 1)

;; a a separator between line number and text
(setq linum-format "%4d ")

;; shortcut for goto-line
(global-set-key "\C-l" 'goto-line)

(put 'downcase-region 'disabled nil)

(load "~/.emacs.d/cpp_tempo.el")

(add-hook 'c++-mode-hook
	   (lambda() (local-set-key (kbd "C-n") 'tempo-template-c-main)))
