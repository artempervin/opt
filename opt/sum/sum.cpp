#define BOOST_TEST_MODULE Test
#include <boost/test/included/unit_test.hpp> 

#include <boost/accumulators/statistics/sum_kahan.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <vector>
#include <algorithm>

template <typename T>
T avg(const std::vector<T>& v)
{
   using namespace boost::accumulators;

   accumulator_set<float, features<tag::sum_kahan>> acc;
   std::for_each (v.begin(), v.end(), std::ref(acc));

   auto sum = sum_kahan(acc);
   return sum/v.size();
}

BOOST_AUTO_TEST_CASE(test_positive)
{
   std::vector<float> v = {1, 2, 3, 4, 5};
   BOOST_CHECK_EQUAL(3, avg(v));
}

BOOST_AUTO_TEST_CASE(test_negative)
{
   std::vector<float> v = {-1, -2, -3, -4, -5};
   BOOST_CHECK_EQUAL(-3, avg(v));
}

BOOST_AUTO_TEST_CASE(test_zero_sum)
{
   std::vector<float> v = {-1, 1, -2, 2};
   BOOST_CHECK_EQUAL(0, avg(v));
}

BOOST_AUTO_TEST_CASE(test_fractionals1)
{
   std::vector<float> v = {1, 1e-6, 1e6};
   BOOST_CHECK_CLOSE(333333666667/1000000.0, avg(v), 1e-5);
}

BOOST_AUTO_TEST_CASE(test_fractionals2)
{
   std::vector<float> v = {3/2.0, 4/5.0, 9/7.0};
   BOOST_CHECK_CLOSE(251/210.0, avg(v), 1e-5);
}

