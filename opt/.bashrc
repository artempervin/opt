# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
#    source ~/opt/cut_prompt/cut_prompt.sh
  PROMPT_COMMAND='DIR=`pwd|sed -e "s!$HOME!~!"`; 
  if [ ${#DIR} -gt 30 ]; then 
    CurDir=${DIR:0:12}...${DIR:${#DIR}-15}; 
  else 
    CurDir=$DIR; 
  fi'

  if [ `whoami` == "root" ]; then
    UserLabel="#"
  else
    UserLabel="$"
  fi

  PS1="[\W]\$UserLabel "
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    shopt -s cdspell

    eval "`dircolors -b`"
    alias E='emacs -nw -fn 6x13'
    alias cd='pushd'
    alias grep='grep --color=auto'

    pushd()
    {
        builtin pushd "$@" > /dev/null
    }
fi

# some more ls aliases
alias ls='ls -Blah --color=auto'
alias l='ls'
alias la='ls'
alias lls='ls'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

export SVN_EDITOR="emacs -nw"
export EDITOR="emacs"
export LS_COLORS="no=00:fi=00:di=00;34:ln=00;36:pi=40;33:so=00;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:ex=00;32:*.cmd=00;32:*.exe=00;32:*.com=00;32:*.btm=00;32:*.bat=00;32:*.sh=00;32:*.csh=00;32:*.tar=00;31:*.tgz=00;31:\
*.arj=00;31:*.taz=00;31:*.lzh=00;31:*.zip=00;31:*.z=00;31:*.Z=00;31:*.gz=00;31:*.bz2=00;31:*.bz=00;31:*.tz=00;31:*.rpm=00;31:*.cpio=00;31:*.jpg=00;35:*.gif=00;35:*.bmp=00;35:*.xbm=00;35:*.xpm=00;35:*.png=00;35:*.tif=00;35:"

######################################################################
FIND_FILTER_FILES="-not -iwholename ‘*.o’ -not -iwholename ‘*.d’ -not -iwholename ‘*~’" 

function findstring () 
{
  IGNORE_CASE=-i
  if [ -z “$3” ]; then
    IGNORE_CASE=””
  fi

  find . -name 'build' -prune -o -regex “.*\.\($1\)” -type f -print0 | xargs -0 grep -n $(IGNORE_CASE) “$2” {} \; 2> /dev/null
}

function fcc()
{
    findstring “cpp\|h” “$1” “$2” | tr 'c++' 'cpp'
}

function ffci()
{
  fcc $1 "ignore case"    
}

function ff()
{
    find . -type f -iname ‘*’$*’*’ $FIND_FILTER_FILES -print | tr 'c++' 'cpp'
}

alias finderror="grep -i error build.log | grep -v \"No error\" | grep -v \"[ERROR\]\" | grep -v \"Ignore any earlier messages about initialization\" | grep -v \"\-\-\-\- Expect\" | grep -v \"\-\-\-\- We should\" | grep -v \"\-Werror\=switch\""

function make()
{
  if [ -f Makefile ]; then
    /usr/bin/make @>&1 -j 6 DEBUG=1 $1 | tee build.log
    if [[ $? -ne 0 ]]; then
      finderror
    fi
  else
    echo "No Makefile found."
  fi
}

function strreplace()
{
  MATCH=$1
  REPLACE=$@
  
  FILES=$(eval "/bin/find . -type f -regex '.*\.\(cpp\|h\)' $FIND_FILTER_FILES | tr 'c++' 'cpp' | xargs grep -l $MATCH")
  
  for f in ${$FILES[@]}; do
    sed -i "s/$MATCH/$REPLACE/g" $f
  done
  
  echo "Replaced [$MATCH] to [$REPLACE] in ${#FILES[@]} files"
}

# unset limits for core files
ulimit -c unlimited
