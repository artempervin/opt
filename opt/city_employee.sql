drop table if exists city;
drop table if exists employee;

create table city (id int, name text, county text);
create table employee (name text, cityId int, salary int);

insert into city values (1, "Moscow", "Russia");
insert into city values (2, "SPb", "Russia");
insert into city values (3, "New York", "USA");
insert into city values (4, "Paris", "France");
insert into city values (5, "Stockholm", "Sweden");

insert into employee values ("Ivan", 1, 100500);
insert into employee values ("Petr", 1, 42);
insert into employee values ("Michael", 3, 0);
insert into employee values ("Louis", 4, -1);
insert into employee values ("Martin", 6, 999);
insert into employee values ("Oleg", 1, 1024);
insert into employee values ("Maria", 2, 2048);
insert into employee values ("Anna", 2, 4096);

select c.name, count(e.cityId) as count
  from city as c 
  left join employee as e on c.id=e.cityId
  group by e.cityId 
  order by c.name;

--https://kripken.github.io/sql.js/GUI/